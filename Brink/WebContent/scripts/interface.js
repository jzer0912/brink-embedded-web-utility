
(function() {

	$(document).ready(function() {


		if (navigator.userAgent.match(/iPad/i)) {
			//code for iPad here 
		}

		if (navigator.userAgent.match(/iPhone/i)) {
			var viewportmeta = document.querySelector('meta[name="viewport"]');
			viewportmeta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';
		}


		if (navigator.userAgent.match(/Android/i)) {
			var viewportmeta = document.querySelector('meta[name="viewport"]');
			viewportmeta.content = 'width=device-width, initial-scale=.9, maximum-scale=.9, user-scalable=no';
		}



		if (navigator.userAgent.match(/BlackBerry/i)) {
			//code for BlackBerry here 
		}


		if (navigator.userAgent.match(/Mozilla/i)) {
			//code for webOS here 
			var x = 0;
		}

		$.getJSON('/menuContent/menuStruct.json', function(result) {

			Object.keys(result).forEach(function(key, keyIndex) {
				msg = "<li id=" + key + "> <a href=>" + key + "</a></li>";
				document.querySelector('#settingsList').innerHTML += msg;
			});
			menuItems = result;
			$('#settingsList').listview('refresh');
			//setButtons();

		});

		var isTouchDevice = 'ontouchstart' in document.documentElement;


		var tapFlag = true;
		$(document).on("scrollstart", function() {
			tapFlag = false;
		});
		$(document).on("scrollstop", function() {
			setTimeout(function() {
				tapFlag = true;
			}, 2000);
		});



		$('#settingsList').on('touchend', function() {
			if (isTouchDevice) {
				var selected = event.target;
				if (tapFlag) {
					if (selected.text && isTouchDevice) {


						nextList(selected.text);
						var selected = "";
					}
				}
			}
		});

		$('#settingsList').mousedown(function(event) {
			var selected = event.target;
			if (selected.text && isTouchDevice == false) {


				nextList(selected.text);
			}
		});
		$('#circleUI').mousedown(function(event) {
			var selected = event.target.id;
			if (isTouchDevice == false) {
				pressbutton(selected);
			}
		});
		$('#circleUI').mouseup(function(event) {
			if (isTouchDevice == false) {
				var selected = event.target.id;
				upButton(selected);
			}
		});
		$('#circleUI').mouseleave(function(event) {
			if (isTouchDevice == false) {
				var selected = event.target.id;
				upButton(selected);
			}
		});

		$('#settingsList').on('swipeleft', function() {
			var selected = event.target;
			if (selected.text && isTouchDevice) {


				nextList(selected.text);
				var selected = "";
			}
		});





		$('#circleUI').on('touchstart', function() {
			if (isTouchDevice) {
				var selected = event.target.id;
				pressbutton(selected);
			}
		});
		$('#circleUI').on('touchend', function() {
			if (isTouchDevice) {
				var selected = event.target.id;
				upButton(selected);
			}
		});
	}
	);

})();


var image_tracker = 'orange';
var _current = 1;
var _count = 1;
var menuItems = {};
var modLock = true;
var optLock = false;
var showList = false;
//Create a table element dynamically
var table = document.createElement("table");

//Create a select element dynamically
var select = document.createElement("select");

//Create a option element dynamically
var option = document.createElement("option");
var lastMenu = "";
var uiLevel = 0;
var listMain = [
	"1 Debiet",
	"2 Bypass",
	"3 Vorstbeveiliging",
	"4 Filtermelding",
	"5 Externe verwarmer",
	"6 CO2 sensor",
	"7 Vochtsensor",
	"8 Cascade",
	"9 Schakelcontacten",
	"10 0-10 V",
	"11 Aardwarmtewisselaar",
	"12 CV+WTW",
	"13 Netwerk",
	"14 Communicatie",
	"15 Algemene instellingen",
	"16 Standby"
];

var UID = {
	getNew : function() {
		if (this._current == 2) {
			this._current = 1;
			_count++;
		} else {
			this._current = 2;
			_count++;
		}

		return this._current;
	}
};

$(window).on("orientationchange", function(e) {

	setTimeout(function() {
		$(window).resize();
	}, 150);


});
$(document).on("pagecontainershow", function(e, data) {

	$(window).resize();

});




$(window).on("resize", function(e) {
drawUI();
});


function drawUI() {
	var screenHeight = $.mobile.getScreenHeight();
	var screenWidth = $(window).width();



	var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight() - 1 : $(".ui-header").outerHeight(),
		contentCurrentHeight = $(".ui-content").outerHeight() - $(".ui-content").height(),
		contentCurrentWidth = $(".ui-content").outerWidth() - $(".ui-content").width(),
		contentHeight = screenHeight - header - contentCurrentHeight,
		contentWidth = screenWidth - header - contentCurrentWidth;


	var column1 = document.getElementById("column1");
	var column2 = document.getElementById("column2");
	var column3 = document.getElementById("column3");
	var menuPath = document.getElementById("menuPath");
	var iconTable = document.getElementById("iconTable");
	var notifTable = document.getElementById("notifTable");
	var iconsTbl = document.getElementById("iconsTbl");
	var settingsList = document.getElementById("settingsList");
	var menuTable = document.getElementById("menuTable");
	var logoP = document.getElementById("logoP");
	var logoL = document.getElementById("logoL");
	var circleInterface = document.getElementById("circleInterface");
	var icons = document.getElementById("icons");
	var row = document.getElementById("row");
	row.style.height = contentHeight + "px";
	$(".ui-content").height(contentHeight);
	if (screenHeight > screenWidth) {
//portrait
		

		column1.style.minHeight = "";
		column2.style.minHeight = "";
		column3.style.minHeight = "";
		column1.style.maxHeight = "";
		column2.style.maxHeight = "";
		column3.style.maxHeight = "";
		column1.style.width = "100%";
		column2.style.width = "100%";
		column3.style.width = "100%";
		column1.style.height = "10%";
		column2.style.height = "45%";
	
		iconTable.style.verticalAlign = "top";
		notifTable.style.verticalAlign = "top";
		$(".icons").attr("align", "left");
		$(".notifications").attr("align", "right");
		circleInterface.style.marginTop = "-25px";
		iconsTbl.style.width = "100%";
		if(showList)
		{
			menuPath.style.paddingLeft = "10px"; 
		menuPath.style.paddingTop= "10px";
		menuPath.style.paddingBottom= "30px"; 
		menuPath.style.margin= "5px 10px 0px 0px"; 
		menuPath.style.height= "30px";
			menuPath.style.width = "100%";
			logoP.style.display = "none";
			column2.style.height =contentHeight -60+"px";
		settingsList.style.height =  contentHeight+"px";
		settingsList.style.width = column2.clientWidth-60+"px";
		$(".ui-listview").width(screenWidth+10);
		$(".ui-listview").height(contentHeight-110);
		column3.style.height = "0px";
		settingsList.style.marginTop = "0px";
		}
	else
		{
		logoP.style.display = "table-cell";
		logoL.style.display = "none";
		column1.style.minHeight = "";
		column2.style.minHeight = "";
		column3.style.minHeight = "";
		column1.style.maxHeight = "";
		column2.style.maxHeight = "";
		column3.style.maxHeight = "";
		column3.style.height = "25%";
		column1.style.height = "25%";
		column2.style.height = "45%";
		}
	} else {

		//landscape
		

		$(".notifications").attr("align", "right");
		circleInterface.style.marginTop = "-10px";
		iconTable.style.verticalAlign = "middle";
		$(".icons").attr("align", "right");
		notifTable.style.verticalAlign = "bottom";
		$(".notifications").attr("align", "left");
	
		column1.style.minHeight = "260px";
		column2.style.minHeight = "60px";
		column3.style.minHeight = "250px";
		column1.style.maxHeight = "35%";
		column2.style.maxHeight = "35%";
		column3.style.maxHeight = "35%";
		column1.style.height = "100%";
		column2.style.height = "100%";
		column3.style.height = "100%";
		logoP.style.display = "none";
		logoL.style.display = "table-cell";
		iconsTbl.style.width = "60px";
		if(showList)
		{
			column2.style.width = screenWidth -71+"px";
	
			menuPath.style.marginTop = "0px";
			menuPath.style.paddingTop = "5px";
			menuPath.style.paddingBottom = "12px";
			menuPath.style.paddingLeft = "11px";
			menuPath.style.zIndex = "20";
			logoL.style.display = "none";
			menuPath.style.width = "84%";
			$(".ui-listview").width(screenWidth+50);
			$(".ui-listview").height(contentHeight);
			settingsList.style.marginTop = "0px";
			settingsList.style.paddingLeft = "17px";
			settingsList.style.height =  contentHeight-40+"px";
			settingsList.style.width = screenWidth-55+"px";
		column3.style.width = "0px";
		column1.style.width = "60px";
		column1.style.marginRight = "9px";
		}
	else
		{
		column1.style.marginRight = "";
		logoL.style.display = "table-cell";
		column2.style.width = "40%";
		column3.style.width = "30%";
		column1.style.width = "30%";
		column1.style.height = "100%";
		column2.style.height = "100%";
		column3.style.height = "100%";
		}
	
	}

	//row.style.width = contentWidth;


}

HTMLElement.prototype.pseudoStyle = function(element, prop, value) {
	var combo = document.getElementById("pseudoStyles");
	if (combo != null && _count > 2) {
		combo.textContent = "";
		_count = 1;
	}
	var _this = this;
	var _sheetId = "pseudoStyles";
	var _head = document.head || document.getElementsByTagName('head')[0];
	var _sheet = document.getElementById(_sheetId) || document.createElement('style');
	_sheet.id = _sheetId;
	if (_head == null) return 1;
	while (_head.length > 0) {
		_head.removeChild(_head.childNodes[0])
	}
	var className = "pseudoStyle" + UID.getNew();

	_this.className += " " + className;

	_sheet.innerHTML += "\n." + className + ":" + element + "{" + prop + ":" + value + "}";
	_head.appendChild(_sheet);
	return this;
};

function startTime() {
	var today = new Date();
	var date = today.getUTCDate();
	var month = today.getUTCMonth() + 1;
	var year = today.getFullYear();
	var strDate = "" + date + "/" + month + "/" + year;
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	document.getElementById('currTime').innerHTML = h + ":" + m + ":" + s;
	document.getElementById('currDate').innerHTML = strDate;
	var t = setTimeout(startTime, 500);
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i
	}
	; // add zero in front of numbers < 10
	return i;
}



function setButtons() {
	var buttons = [];
	buttons.push(document.querySelector('#button1'));
	buttons.push(document.querySelector('#button2'));
	buttons.push(document.querySelector('#button3'));
	buttons.push(document.querySelector('#button4'));

	for (var i = 0; i < 4; ++i) {
		var incr = i.toString(10);
		buttons[i].textContent = menuItems["1 Flow"]["list"]["Setting " + incr]["properties"]["value"];
	}
}
function nextList(selected) {
	var selector = document.querySelector('#settingsList');

	var menuPath = document.getElementById("menuPath");
	var pathArray = menuPath.textContent.split("\\");
	selector.innerHTML = "";
	for (i = 1; i < selector.length; i++) {
		selector.removeChild(selector.childNodes[i]);
	}
	if (optLock) {
		setOption(selected);
	} else {


		if (menuPath.textContent != "Instelmenu") {


			var i = 0;
			var found = false;
			while (i < pathArray.length && !found) {

				if (menuItems[pathArray[i]].list.hasOwnProperty(selected)) {
					if (menuItems[pathArray[i]].list[selected].hasOwnProperty("options")) {
						var options = Object.keys(menuItems[pathArray[i]].list[selected].options);
						var values = Object.values(menuItems[pathArray[i]].list[selected].options);
						for (var j = 0; j < options.length; j++) {

							if (values[j]) {
								msg = "<li  id=\"selLI\"> <a href  id=\"selLI\">" + options[j] + "</a></li>";
								selector.innerHTML += msg;


							} else {
								msg = "<li data-theme=\"d\" id=" + options[j] + "> <a href=>" + options[j] + "</a></li>";
								selector.innerHTML += msg;
							}


						}

						optLock = true;
					} else if (menuItems[pathArray[i]].list[selected].hasOwnProperty("properties")) {
						var properties = Object.keys(menuItems[pathArray[i]].list[selected].properties);
						var values = Object.values(menuItems[pathArray[i]].list[selected].properties);
						var inputDiv = document.getElementById("inputDiv");
						var menuUI = document.getElementById('settingsList');
						var valueDisplay = document.getElementById('valueDisplay');
						var scale = document.getElementById('scale');
						if (inputDiv.style.display === "none") {
							inputDiv.style.display = "block";
							menuUI.style.display = "none";

							if (values[4] == "C"){
								scale.textContent =  'C';
							}
							else
								{
								scale.textContent = values[4];
								}
							if (values[5] == 'i') {
								valueDisplay.textContent = values[0];
							} else {
								valueDisplay.textContent = parseFloat(values[0]).toFixed(1);
							}


						} else {
							inputDiv.style.display = "none";
							menuUI.style.display = "block";
						}



					} else {

						var inputDiv = document.getElementById("inputDiv");
						var menuUI = document.getElementById('settingsList');
						if (inputDiv.style.display === "none") {
							inputDiv.style.display = "block";
							menuUI.style.display = "none";
						} else {
							inputDiv.style.display = "none";
							menuUI.style.display = "block";
						}

					}
					found = true;
					menuPath.textContent = menuPath.textContent + "\\" + selected;
				}
				++i;
			}
		} else {

			var backIcon = document.getElementById('backIcon');
			backIcon.style.display = "inline";
			menuPath.textContent = selected;
			var options = Object.keys(menuItems[selected].list);

			for (i = 0; i < options.length; i++) {


				msg = "<li id=" + options[i] + "> <a href=>" + options[i] + "</a></li>";
				selector.innerHTML += msg;
				var inputDiv = document.getElementById("inputDiv");
				var menuUI = document.getElementById('settingsList');
				inputDiv.style.display = "none";
				menuUI.style.display = "block";


			}

		}
	}
	$('#settingsList').listview('refresh');


}

function setOption(selected) {
	var menuPath = document.getElementById("menuPath");
	var selector = document.querySelector('#settingsList');
	var pathArray = menuPath.textContent.split("\\");
	var options = Object.keys(menuItems[pathArray[0]].list[pathArray[1]].options);
	var values = Object.values(menuItems[pathArray[0]].list[pathArray[1]].options);
	selector.innerHTML = "";
	for (i = 1; i < selector.length; i++) {
		selector.removeChild(selector.childNodes[i]);
	}
	
	for (var i = 0; i < options.length; i++) {
		if (options[i] ==selected){
			values[i] = true; 
		}
		else {
			values[i] = false; 
		}
	}
for (var j = 0; j < options.length; j++) {

							if (values[j]) {
								msg = "<li  id=\"selLI\"> <a href  id=\"selLI\">" + options[j] + "</a></li>";
								selector.innerHTML += msg;


							} else {
								msg = "<li data-theme=\"d\" id=" + options[j] + "> <a href=>" + options[j] + "</a></li>";
								selector.innerHTML += msg;
							}


						}
						$('#settingsList').listview('refresh');
}

function modify(type) {
	if (!modLock) {
		var menuPath = document.getElementById("menuPath");
		var pathArray = menuPath.textContent.split("\\");

		var properties = Object.keys(menuItems[pathArray[0]].list[pathArray[1]].properties);
		var values = Object.values(menuItems[pathArray[0]].list[pathArray[1]].properties);
		var valueDisplay = document.getElementById('valueDisplay');
		if (type == 'i') {

			if (values[5] == 'i') {



				if (parseInt(valueDisplay.textContent) < values[1]) {
					valueDisplay.textContent = parseInt(valueDisplay.textContent) + values[3];

				}
			} else {
				if (parseFloat(valueDisplay.textContent) < parseFloat(values[1])) {

					valueDisplay.textContent = (parseFloat(valueDisplay.textContent) + parseFloat(values[3])).toFixed(1);

				}
			}
		} else {
			if (values[5] == 'i') {



				if (parseInt(valueDisplay.textContent) > values[2]) {
					valueDisplay.textContent = parseInt(valueDisplay.textContent) - values[3];

				}
			} else {

				if (parseFloat(valueDisplay.textContent) > parseFloat(values[2])) {

					valueDisplay.textContent = (parseFloat(valueDisplay.textContent) - parseFloat(values[3])).toFixed(1);

				}
			}

		}

		menuItems[pathArray[0]].list[pathArray[1]].properties.value = parseInt(valueDisplay.textContent);
		setButtons();
	} else {
		modLock = false;
	}
}

function backMenu() {
	optLock = false;
	modLock = true;
	var menuPath = document.getElementById("menuPath");
	var pathArray = menuPath.textContent.split("\\");
	if (pathArray.length > 1) {

		menuPath.textContent = "Instelmenu";
		nextList(pathArray[pathArray.length - 2]);
	} else {
		menuPath.textContent = "Instelmenu";
		var backIcon = document.getElementById('backIcon');
		backIcon.style.display = "none";
		document.querySelector('#settingsList').innerHTML = "";

		Object.keys(menuItems).forEach(function(key, keyIndex) {
			msg = "<li id=" + key + "> <a href=>" + key + "</a></li>";
			document.querySelector('#settingsList').innerHTML += msg;
		});
		$('#settingsList').listview('refresh');
	}
}

function homeMenu() {
	showList = false;
	modLock = true;
	var circleUI = document.getElementById("circleUI");
	var menuPath = document.getElementById("menuPath");
	menuPath.textContent = "Instelmenu";
	var backIcon = document.getElementById('backIcon');
	backIcon.style.display = "none";
	var infoIcon = document.getElementById('infoIcon');
	var info = document.getElementById("info");
	info.style.display = "none";
	var homeIcon = document.getElementById('homeIcon');
	var inputDiv = document.getElementById('inputDiv');
	var settingsIcon = document.getElementById('settingsIcon');
	var menuPath = document.getElementById('menuPath');
	var settingsList = document.getElementById("settingsList");
	var circleInterface = document.getElementById("circleInterface");
	var column3 = document.getElementById("column3");
	circleInterface.style.display = "block";
	column3.style.display = "block";
	inputDiv.style.display = "none";
	homeIcon.style.display = "none";
	settingsIcon.style.display = "inline";
	infoIcon.style.display = "inline";
	document.querySelector('#settingsList').innerHTML = "";
	Object.keys(menuItems).forEach(function(key, keyIndex) {
		msg = "<li id=" + key + "> <a href=>" + key + "</a></li>";
		document.querySelector('#settingsList').innerHTML += msg;
	});
	$('#settingsList').listview('refresh');
		drawUI();
		circleUI.style.display = "block";
		settingsList.style.display = "none";
		menuPath.style.display = "none";
}

function settingsMenu() {
	
	showList = true;
	var homeIcon = document.getElementById('homeIcon');
	var settingsIcon = document.getElementById('settingsIcon');
	var menuPath = document.getElementById('menuPath');
	var settingsList = document.getElementById("settingsList");
	var circleInterface = document.getElementById("circleInterface");
	var column3 = document.getElementById("column3");
	var infoIcon = document.getElementById('infoIcon');
	circleInterface.style.display = "none";
	column3.style.display = "none";

	homeIcon.style.display = "inline";
	settingsIcon.style.display = "none";
	infoIcon.style.display = "none";
		drawUI();


		settingsList.style.display = "inline-block";
		menuPath.style.display = "inline-block";
}

function showInfo() {
	var circleUI = document.getElementById("circleUI");
	var info = document.getElementById("info");


	var homeIcon = document.getElementById('homeIcon');
	var settingsIcon = document.getElementById('settingsIcon');
	var infoIcon = document.getElementById('infoIcon');

	info.style.display = "block";
	circleUI.style.display = "none";
	homeIcon.style.display = "inline";
	infoIcon.style.display = "none";
	settingsIcon.style.display = "none";
}


function change() {
	var image = document.getElementById('ur_circle');
	if (image_tracker == 'orange') {
		image.src = 'icons/button_off_unpressed.bmp';

		image_tracker = 'blue';
	} else {

		image.src = 'icons/button_off_pressed.bmp';
		image_tracker = 'orange';
	}
}


function addCombo() {
	var textb = document.getElementById("txtCombo");
	var combo = document.getElementById("combo");

	var option = document.createElement("option");
	option.text = textb.value;
	option.value = textb.value;
	try {
		combo.add(option, null); //Standard 
	} catch (error) {
		combo.add(option); // IE only
	}
	textb.value = "";
}

function pressbutton(id) {
	document.getElementById(id).style.background = "DimGrey";
	var div = document.getElementById("circleUI");
	switch (id) {
	case "button1":
		div.style.borderColor = "grey red grey grey";
		div.children[1].style.border = " 1px solid red";
		if (uiLevel < 1) {
			div.pseudoStyle("before", "border-color", "grey red grey grey");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph1.bmp\")");
			uiLevel = 1;
		} else {
			uiLevel = 0;
			div.pseudoStyle("before", "border-color", "grey grey grey grey");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph0.bmp\")");
		}
		break;
	case "button2":
		div.style.borderColor = "grey grey red grey";
		div.children[2].style.border = " 1px solid red";
		if (uiLevel < 2) {
			div.pseudoStyle("before", "border-color", "grey red red grey");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph2.bmp\")");
			uiLevel = 2;
		} else {
			--uiLevel;
			div.pseudoStyle("before", "border-color", "grey red grey grey");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph1.bmp\")");
		}
		break;
	case "button3":
		div.style.borderColor = "grey grey grey red";
		div.children[3].style.border = " 1px solid red";
		if (uiLevel < 3) {
			div.pseudoStyle("before", "border-color", "grey red red red");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph3.bmp\")");
			uiLevel = 3;
		} else {
			--uiLevel;
			div.pseudoStyle("before", "border-color", "grey red red grey");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph2.bmp\")");
		}
		break;
	case "button4":
		div.style.borderColor = "red grey grey grey";
		div.children[0].style.border = " 1px solid red";
		if (uiLevel < 4) {
			div.pseudoStyle("before", "border-color", "red red red red");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph4.bmp\")");
			uiLevel = 4;
		} else {
			--uiLevel;
			div.pseudoStyle("before", "border-color", "grey red red red");
			div.pseudoStyle("after", "background-image", "url(\"icons/graph3.bmp\")");
		}
		break;
	}
}

function pressDown(id){
	buttPressed = document.getElementById('id');
	buttPressed.style.border = "2px";
	buttPressed.style.borderColor = "rgb(255,0,0)";
}

function release(id){
	
}


function upButton(id) {
	var buttonId;
	for (var i = 1; i <= 4; ++i) {
		buttonId = "button" + i

		document.getElementById(buttonId).style.background = "rgb(218,218,218)";
	}
	var div = document.getElementById("circleUI");
	div.children[0].style.border = " 1px solid grey";
	div.children[1].style.border = " 1px solid grey";
	div.children[2].style.border = " 1px solid grey";
	div.children[3].style.border = " 1px solid grey";
	div.style.borderColor = "grey grey grey grey";




}